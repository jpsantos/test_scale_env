# Create your views here.
from django.http.response import HttpResponse

from hardload.tasks import load

def test_celery(request):

    if 'repeat' in request.GET:
        repeat = int(request.GET['repeat'])
        load.delay(repeat)
    else:
        load.delay(1000)

    return HttpResponse('OK', status=200)