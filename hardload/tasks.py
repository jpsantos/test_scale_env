__author__ = 'jpsantos'
from celery import task

print 'LOAD TASKS'


@task()
def add(x, y):
    print 'Start Add'
    for i in xrange(10000):
        pass
    print 'Return Add'
    return x + y

@task()
def load(repeat):
    print 'Start Load'
    results = []
    for i in xrange(repeat):
        results.append(add.delay(i, i))

    keep_working = True

    while keep_working:
        keep_working = False
        for result in results:
            if not result.ready():
                keep_working = True
            else:
                if result.successful():
                    print "result: {0}".format(result.result)
                else:
                    print "error"

    print 'Return Load'
